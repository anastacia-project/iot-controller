# Use an official Python runtime as a parent image
FROM python:3

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN apt update && apt install -y openjdk-11-jdk
RUN pip install --trusted-host pypi.python.org -r requirements.txt # && apt-get update && apt-get install nmap -y

# Make port 80 available to the world outside this container
EXPOSE 8003

# Define environment variable
#ENV NAME World

# Run app.py when the container launches
#CMD ["python", "iot_controller/manage.py", "runserver", "0.0.0.0:8003"] 
CMD ["/app/docker-entrypoint.sh"] 