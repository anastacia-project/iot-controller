#!/bin/bash

# IoT Controller entrypoint

#database="/app/iot_controller/db.sqlite3"
#if [ -f $file ]
#	then
#		echo "$file found, using the current database."
#	else
#		echo "$file not found, regenerating database."
#		cd /app/iot_controller
		#ls
#		regenerate_database.sh
#		cd ..
		#ls
#fi

cd iot_controller
./regenerate_database.sh

python manage.py runserver 0.0.0.0:8003 