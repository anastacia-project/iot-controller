#!/bin/bash

 USAGE=" USAGE: sudo ./1-build_docker.sh iot_controller"

if [ $# -eq 0 ]
  then
    echo $USAGE
    exit 1
fi

docker build -t $1 .
