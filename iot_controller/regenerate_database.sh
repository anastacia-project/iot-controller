#!/bin/bash

find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
find . -path "*/migrations/*.pyc"  -delete

rm db.sqlite3

echo "Makemigrations..."

python manage.py makemigrations && \
python manage.py migrate