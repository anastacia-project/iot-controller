# # -*- coding: utf-8 -*-
# views.py
"""
This python module implements the iot controller health check inside the ANASTACIA European Project,
extending the HSPL/MSPL language defined in secured project.
How to use:
	Request for editor tool: http://anastacia-framework:8005/editor/
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

from rest_framework.views import APIView
from rest_framework.response import Response

class Health(APIView):

	def get(self, request, format=None):
		return Response({"status":"OK"})
