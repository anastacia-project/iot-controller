# # -*- coding: utf-8 -*-
# models.py
"""
This python module implements the iot_controller models inside the ANASTACIA European Project,
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

from django.db import models


class IoTOS(models.Model):
	name = models.TextField()
	version = models.TextField()


class IoTDevice(models.Model):
	device_id = models.IntegerField(unique=True)
	name = models.TextField()
	model = models.TextField()
	os = models.ForeignKey(IoTOS, on_delete=models.CASCADE)
	x_location = models.DecimalField(max_digits=9, decimal_places=6)
	y_location = models.DecimalField(max_digits=9, decimal_places=6)
	creation_date = models.DateTimeField(auto_now_add=True)

	#class Meta:
		#unique_together = ('device_id', 'name', 'model')
		#unique = ('device_id')


class IoTDeviceResource(models.Model):
	resource = models.TextField()
	description = models.TextField()
	iot_device = models.ForeignKey(IoTDevice,related_name='resources', on_delete=models.CASCADE)
	
	def __str__(self):
		return "{}".format(self.resource)

class IoTDeviceResourceMethod(models.Model):
	method = models.TextField()
	payload = models.TextField()
	resource = models.ForeignKey(IoTDeviceResource,related_name='methods', on_delete=models.CASCADE)

class IoTDeviceSoftware(models.Model):
	software_id = models.IntegerField()
	name = models.TextField()
	version = models.TextField()
	iot_device = models.ForeignKey(IoTDevice, related_name='softwares', on_delete=models.CASCADE)
	port = models.IntegerField()


"""
class IoTDeviceLocation(models.Model):
	x = models.DecimalField(max_digits=9, decimal_places=6)
	y = models.DecimalField(max_digits=9, decimal_places=6)
	creation_date = models.DateTimeField(auto_now_add=True)
"""


class IoTNet(models.Model):
	name = models.TextField()
	address = models.TextField()	

	def __str__(self):
		return "{}".format(self.address)

class IoTDeviceIf(models.Model):
	name = models.TextField()
	mac_addr = models.TextField()
	ip_addr = models.TextField()
	iot_net = models.ForeignKey(IoTNet, on_delete=models.CASCADE)
	iot_device = models.ForeignKey(IoTDevice, related_name='ifaces', on_delete=models.CASCADE)
	if_id = models.IntegerField()



