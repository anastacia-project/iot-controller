# # -*- coding: utf-8 -*-
# views.py
"""
This python module implements the iot-controller views inside the ANASTACIA European Project,
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics, filters
import django_filters
from django.db import transaction
from coapthon.client.helperclient import HelperClient
from .models import IoTDevice, IoTNet, IoTDeviceIf, IoTOS, IoTDeviceSoftware, IoTDeviceResource, IoTDeviceResourceMethod
from .serializers import IoTDeviceSerializer
import json
import threading
from datetime import datetime

# For external dtls client execution
from subprocess import check_output, TimeoutExpired, STDOUT, CalledProcessError


# Remote kafka logging
import os
from iot_controller.logger.handlers import KafkaHandler
import logging
logging.basicConfig(level=logging.INFO)
logging.info("STARTING IOT CONTROLLER")

logger = logging.getLogger(__name__)

# remote logging
remote_logger = logging.getLogger("remote_logger")
remote_logger.setLevel(logging.INFO)
remote_logger.propagate = False
# Set remote logging handler
#REMOTE_LOGGING_URL = ['172.17.0.1:9092']
REMOTE_LOGGING_URL = ["{}:{}".format(os.environ["RL_ADDR"],os.environ["RL_PORT"])]
TOPIC = 'anastacia-log'


def init_remote_logging():
	if not len(remote_logger.handlers):
		try:
			logger.info(remote_logger.handlers)
			kh = KafkaHandler(REMOTE_LOGGING_URL, TOPIC)
			remote_logger.addHandler(kh)
		except Exception as e:
			logging.info(e)


'''
class IoTDeviceViewSetAPI(viewsets.ModelViewSet):
	"""
	API endpoint that allows IoTDevices instances to be viewed or edited.
	"""
	queryset = IoTDevice.objects.all()
	serializer_class = IoTDeviceSerializer
	filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
'''

class IoTDeviceFilter(django_filters.FilterSet):
	#max_id = django_filters.NumberFilter(name="id", lookup_expr='lte')
	#closed_before = django_filters.DateFilter(name="close_date", lookup_expr='lte')
	#closed_after = django_filters.DateFilter(name="close_date", lookup_expr='gte')
	#expire_before = django_filters.DateFilter(name="respond_by_date", lookup_expr='lte')
	#expire_after = django_filters.DateFilter(name="respond_by_date", lookup_expr='gte')
	#net = django_filters.CharFilter(name="name", lookup_expr='iexact')
	name = django_filters.CharFilter(lookup_expr='icontains')
	net = django_filters.CharFilter(name="ifaces__iot_net",lookup_expr='address__icontains')
	resource = django_filters.CharFilter(name="resources",lookup_expr='description__icontains')

	class Meta:
		model = IoTDevice
		fields = ['net','name','resource']



class IoTDeviceAPIView(generics.ListAPIView):
	"""
	API endpoint that allows IoTDevices instances to be viewed and filtered.
	"""
	queryset = IoTDevice.objects.all()
	serializer_class = IoTDeviceSerializer
	filter_class = IoTDeviceFilter


class IoTHoneynetAPIView(IoTDeviceAPIView):
	"""
	API endpoint which returns the prepared honeynet in xml.
	"""
	def list(self, request, *args, **kwargs):

		print("get_QUERYSET:",len(self.get_queryset()))
		print("filter_queryset:",len(self.filter_queryset(self.get_queryset())))

		# TODO: Retrieve the content from the database

		iot_honeynet = " ".join("""
			<ioTHoneyNet>
			  <iotns:name>REST with RPL router</iotns:name>
			  <iotns:net id="1">
				<iotns:name>net</iotns:name>
			  </iotns:net>
			  <iotns:router id="1">
				<iotns:name>Wismote RPL Root</iotns:name>
				<iotns:interaction_level>LOW</iotns:interaction_level>
				<iotns:if id="1" net="str1234">
				  <iotns:name>str1234</iotns:name>
				  <iotns:mac_addr>str1234</iotns:mac_addr>
				  <iotns:ip>str1234</iotns:ip>
				</iotns:if>
				<iotns:operatingSystem>
				  <iotns:name>contiki</iotns:name>
				  <iotns:version>2.7</iotns:version>
				</iotns:operatingSystem>
				<iotns:software id="1">
				  <iotns:name>RPL</iotns:name>
				  <iotns:version>3.14159</iotns:version>
				</iotns:software>
				<iotns:model>Wismote</iotns:model>
				<iotns:location>
				  <iotns:x>33.260163187353555</iotns:x>
				  <iotns:y>30.643217359962595</iotns:y>
				</iotns:location>
				<iotns:resource>TEMPERATURE</iotns:resource>
			  </iotns:router>
			  <iotns:ioTHoneyPot id="3002">
				<iotns:name>Erbium Server</iotns:name>
				<iotns:interaction_level>LOW</iotns:interaction_level>
				<iotns:if id="1" net="str1234">
				  <iotns:name>str1234</iotns:name>
				  <iotns:mac_addr>str1234</iotns:mac_addr>
				  <iotns:ip>str1234</iotns:ip>
				</iotns:if>
				<iotns:operatingSystem>
				  <iotns:name>contiki</iotns:name>
				  <iotns:version>2.7</iotns:version>
				</iotns:operatingSystem>
				<iotns:software id="1">
				  <iotns:name>Erbium Server</iotns:name>
				  <iotns:version>3.14159</iotns:version>
				</iotns:software>
				<iotns:model>Wismote</iotns:model>
				<iotns:location>
				  <iotns:x>35.100895239785295</iotns:x>
				  <iotns:y>39.70574552287428</iotns:y>
				</iotns:location>
				<iotns:resource>TEMPERATURE</iotns:resource>
			  </iotns:ioTHoneyPot>
			  <iotns:ioTHoneyPot id="3003">
				<iotns:name>Erbium Server</iotns:name>
				<iotns:interaction_level>LOW</iotns:interaction_level>
				<iotns:if id="1" net="str1234">
				  <iotns:name>str1234</iotns:name>
				  <iotns:mac_addr>str1234</iotns:mac_addr>
				  <iotns:ip>str1234</iotns:ip>
				</iotns:if>
				<iotns:operatingSystem>
				  <iotns:name>contiki</iotns:name>
				  <iotns:version>2.7</iotns:version>
				</iotns:operatingSystem>
				<iotns:software id="1">
				  <iotns:name>Erbium Server</iotns:name>
				  <iotns:version>3.14159</iotns:version>
				</iotns:software>
				<iotns:model>Wismote</iotns:model>
				<iotns:location>
				  <iotns:x>35.100895239785295</iotns:x>
				  <iotns:y>39.70574552287428</iotns:y>
				</iotns:location>
				<iotns:resource>TEMPERATURE</iotns:resource>
			  </iotns:ioTHoneyPot>
			  <iotns:ioTHoneyPot id="3004">
				<iotns:name>Erbium Server</iotns:name>
				<iotns:interaction_level>LOW</iotns:interaction_level>
				<iotns:if id="1" net="str1234">
				  <iotns:name>str1234</iotns:name>
				  <iotns:mac_addr>str1234</iotns:mac_addr>
				  <iotns:ip>str1234</iotns:ip>
				</iotns:if>
				<iotns:operatingSystem>
				  <iotns:name>contiki</iotns:name>
				  <iotns:version>2.7</iotns:version>
				</iotns:operatingSystem>
				<iotns:software id="1">
				  <iotns:name>Erbium Server</iotns:name>
				  <iotns:version>3.14159</iotns:version>
				</iotns:software>
				<iotns:model>Wismote</iotns:model>
				<iotns:location>
				  <iotns:x>35.100895239785295</iotns:x>
				  <iotns:y>39.70574552287428</iotns:y>
				</iotns:location>
				<iotns:resource>TEMPERATURE</iotns:resource>
			  </iotns:ioTHoneyPot>
			</ioTHoneyNet>
		""".split())
		return Response({"honeynet": iot_honeynet})


class RegisterIoTDeviceAPI(APIView):
	"""
	API endpoint that allows IoTDevices instances to be instantiated.
	"""

	@transaction.atomic()
	def post(self, request, format=None):
		
		body = request.stream.read().decode("utf-8")
		#body = request.body
		if not body:
			raise falcon.HTTPBadRequest('Empty request body',
										'A valid JSON document is required.')
		#print("body",body)
		json_input = json.loads(body)

		try:
			# Get OS
			operating_system = json_input["operatingSystem"]
			iot_os,created = IoTOS.objects.get_or_create(name=operating_system["name"],version=operating_system["version"])

			# Get Location
			location = json_input["location"]

			# IoT-device, get or create new with the specified unique ID
			IoTDevice.objects.filter(device_id=json_input["id"]).delete()
			iot_device = IoTDevice(device_id=json_input["id"],name = json_input["name"],model = json_input["model"],
				os = iot_os,x_location = location["x"],y_location = location["y"])
			iot_device.save()

			if_list = json_input["ifaces"]
			# Remove old information and update with the new one
			IoTDeviceIf.objects.filter(iot_device=iot_device).delete()
			for iot_if in if_list:
				# IoT Net, get or create new with the net address string
				iot_net,created = IoTNet.objects.get_or_create(address=iot_if["net"])
				# Store each iot_if
				iot_device_if = IoTDeviceIf(if_id=iot_if["id"],iot_device=iot_device,iot_net=iot_net, name = iot_if["name"],
					mac_addr = iot_if["mac_addr"], ip_addr = iot_if["ip_addr"])
				iot_device_if.save()

			#IoT Software, get the 
			software_list = json_input["software"]
			# Remove old information and update with the new one
			IoTDeviceSoftware.objects.filter(iot_device=iot_device).delete()
			for software in software_list:
				logger.info("Software: {}".format(software))
				soft_obj = IoTDeviceSoftware(software_id=software["id"],iot_device=iot_device, name = software["name"],
					version = software["version"], port = software["port"])
				soft_obj.save()

			# Resources
			resources = json_input["resources"]
			# Remove old information and update with the new one
			IoTDeviceResource.objects.filter(iot_device=iot_device).delete()
			for resource in resources:
				logger.info("Resource: {}".format(resources))
				iot_device_resource = IoTDeviceResource(resource=resource["resource"],description=resource["description"],iot_device=iot_device)
				iot_device_resource.save()
				for method in resource["methods"]:
					iot_device_resource_method = IoTDeviceResourceMethod(method=method["method"],payload=method["payload"],resource=iot_device_resource)
					iot_device_resource_method.save()

			print(iot_device)
		except Exception as e:
			print(str(e))
			return Response({'status':str(e)})

		# Create the hspl-mspl refinement
		return Response({'status':'IoT device has been registered successfully'})


class IoTDeviceResourceAPI(APIView):

	MAX_ATTEMPTS = 3

	def parse_body(self,request):
		body = request.stream.read().decode("utf-8")
		#logger.info("RECEIVED:{}".format(body))
		if not body:
			raise falcon.HTTPBadRequest('Empty request body',
										'A valid JSON document is required.')
		logger.info("Received {}".format(body))
		self.json_body =  json.loads(body)

	def logging_request(self, data):
		init_remote_logging()
		logging_message = {
			"timestamp": datetime.timestamp(datetime.now()),
			"from_module": "SECURITY_ORCHESTRATOR",
			"from_component": "SECURITY_ORCHESTRATOR",
			"to_module": "IOT_CONTROLLER",
			"to_component": "IOT_CONTROLLER",
			"incoming": True,
			"method": "REST",
			"data": json.dumps(data),
			"notes": "IoT Control request"
		}
		remote_logger.info(logging_message)

	def logging_response(self, data):
		init_remote_logging()
		logging_message = {
			"timestamp": datetime.timestamp(datetime.now()),
			"from_module": "IOT_CONTROLLER",
			"from_component": "IOT_CONTROLLER",
			"to_module": "SECURITY_ORCHESTRATOR",
			"to_component": "SECURITY_ORCHESTRATOR",
			"incoming": False,
			"method": "REST",
			"data": data,
			"notes": "IoT Control response"
		}
		remote_logger.info(logging_message)



	def get(self, request, format=None):

		self.parse_body(request)

		self.logging_request(self.json_body)

		logging.info("GET RECEIVED: {}".format(self.json_body))

		# Should it works using a subnet instead a specific device?
		
		target = self.json_body["target"]
		if "/128" in target:
			target = target.split("/128")[0]
		port = int(self.json_body["port"])
		resource = self.json_body["resource"]
		print("Sending request to {}:{}".format(target,port))

		"""
		# COAP WITHOUT DTLS
		client = HelperClient(server=(target, port))
		response = client.get(resource,timeout=5)
		#print(response.pretty_print())
		client.stop()
		"""

		# DTLS: Python does not have PSK features in the API ("1" is required by the jar)
		cmd = ["java", "-jar", "coap_dtls_client/simple_coapdtls_client.jar", "PSK", "GET", "coaps://[{}]:{}/{}".format(target,port,resource)]
		#cmd = "java -jar coap_dtls_client/simple_coapdtls_client.jar PSK GET coaps://[{}]:{}/{}".format(target,port,resource)
		#java -jar simple_coapdtls_client.jar PSK GET "coaps://[2001:720:1710:4::3002]:5684/.well-known/core"
		response = None
		current_attemp = 0
		while current_attemp <= self.MAX_ATTEMPTS:
			try:
				#process = os.popen(cmd)
				#response = process.read()
				response = check_output(cmd, stderr=STDOUT, timeout=7)
				print(response)
				if response:
					response=response.decode("utf-8").split("RAW PARAMETERS")[1]

			#except CalledProcessError as cpe:
			#	print(cpe.cmd,cpe.returncode,cpe.output)

			except TimeoutExpired as te:
				if current_attemp < self.MAX_ATTEMPTS:
					current_attemp=current_attemp+1
					logger.info("Trying again...")
					continue
				response = "Unable to connect to coaps://[{}]:{}\n".format(target,port)
				logger.debug(response)
				current_attemp=current_attemp+1
			except Exception as e:
				response = "Error during enforcement: {}\n".format(e)
				current_attemp=current_attemp+1
				import traceback
				traceback.print_exc()
				logger.debug(response)

			self.logging_response(response)

			return Response(response)

		# Create a response using msgpack
		#resp.data = msgpack.packb(response.pretty_print(), use_bin_type=True)
		#resp.content_type = 'application/msgpack'
		"""
		if response:
			return Response({'status':response.pretty_print()})
			#return Response({'status':response})
		else:
			return Response({'status':'Can not connect to the IoT device'})
		"""

	def put(self, request, format=None):
		self.parse_body(request)

		self.logging_request(self.json_body)
		logging.info("PUT RECEIVED: {}".format(self.json_body))

		# Should it works using a subnet instead a specific device?
		
		target = self.json_body["target"]
		if "/128" in target:
			target = target.split("/128")[0]
		port = int(self.json_body["port"])
		resource = self.json_body["resource"]
		payload = str(self.json_body["payload"])
		print("Sending request to {}:{}".format(target,port))

		"""
		# COAP WITHOUT DTLS
		client = HelperClient(server=(target, port))
		response = client.get(resource,timeout=5)
		#print(response.pretty_print())
		client.stop()
		"""

		# DTLS: Python does not have PSK features in the API ("1" is required by the jar)
		cmd = ["java", "-jar", "coap_dtls_client/simple_coapdtls_client.jar", "PSK", "PUT", "coaps://[{}]:{}/{}".format(target,port,resource), payload, "1"]
		#cmd = "java -jar coap_dtls_client/simple_coapdtls_client.jar PSK GET coaps://[{}]:{}/{}".format(target,port,resource)
		#java -jar simple_coapdtls_client.jar PSK GET "coaps://[2001:720:1710:4::3002]:5684/.well-known/core"
		response = None
		current_attemp = 0
		while current_attemp <= self.MAX_ATTEMPTS:
			try:
				#process = os.popen(cmd)
				#response = process.read()
				response = check_output(cmd, stderr=STDOUT, timeout=7)
				print(response)
				if response:
					response=response.decode("utf-8").split("RAW PARAMETERS")[1]
			#except CalledProcessError as cpe:
			#	print(cpe.cmd,cpe.returncode,cpe.output)
			except TimeoutExpired as te:
				if current_attemp < self.MAX_ATTEMPTS:
					current_attemp=current_attemp+1
					logger.info("Trying again...")
					continue
				logger.info("Unable to connect to coaps://[{}]:{}\n".format(target,port))
				response = "Unable to connect to coaps://[{}]:{}\n".format(target,port)
				logger.debug(response)
				current_attemp=current_attemp+1
			except Exception as e:
				response = "Error during enforcement: {}\n".format(e)
				current_attemp=current_attemp+1
				import traceback
				traceback.print_exc()
				logger.debug(response)
			
			self.logging_response(response)

		return Response(response)




	def put_bulk(self, request, format=None):

		def worker(iot_conf,enforced_devices):
			logger.info("WORKER!")
			
			iot_conf = json.loads(iot_conf)
			target = iot_conf["target"]
			port = int(iot_conf["port"])
			resource = iot_conf["resource"]
			payload = str(iot_conf["payload"])
			enforced_devices[target] = 0
			if "/" in target:
				target = target.split("/")[0]
				#logger.info("SENDING CONFIGURATION TO: {}".format(count))
				#logger.info("Sending request to {}:{}".format(target,port))
			
			try:

				# DTLS: Python does not have PSK features in the API ("1" is required by the jar)
				cmd = ["java", "-jar", "coap_dtls_client/simple_coapdtls_client.jar", "PSK", "PUT", "coaps://[{}]:{}/{}".format(target,port,resource), "1"]
				response = None
				try:
					#process = os.popen(cmd)
					#response = process.read()
					response = check_output(cmd, stderr=STDOUT, timeout=3)
					logger.info(response)
				#except CalledProcessError as cpe:
				#	print(cpe.cmd,cpe.returncode,cpe.output)
				except TimeoutExpired as te:
					response = "Unable to connect to coaps://[{}]:{}\n".format(target,port)
					logger.info(response)
				except Exception as e:
					response = "Error during enforcement: {}\n".format(e)
					import traceback
					traceback.print_exc()
					logger.info(response)


				# CoAP
				"""
				client = HelperClient(server=(target, port),sock=sock)
				response = client.put(resource,payload,timeout=5)
				#print(response.pretty_print())
				client.stop()
				"""
				logger.info("RESPONSE IS {}".format(response))
				if response:
					enforced_devices[target] = response
				#return Response({'status':response.pretty_print()})
				#return Response({'status':'Can not connect to the IoT device'})
			except Exception as e:
				#logger.info(str(e))
				enforced_devices[target] = str(e)
				client.stop()
				#logger.info("ERROR ENFORCING ")	
				#return Response({'status':"500 {}".format(str(e))})


		self.parse_body(request)
		logger.info("RECEIVED:{}".format(self.json_body))
		enforced_devices = {}
		threads = []
		for iot_conf in self.json_body:
			t = threading.Thread(target=worker,args=[iot_conf,enforced_devices])
			threads.append(t)
			t.start()

		finished = False
		while not finished:
			finished = True
			for thread in threads:
				if thread.isAlive():
					finished = False
					break

		logger.info("ENFORCED SUCCESFULLY")	
		#self.logging_response(enforced_devices)
		return Response({'status':json.dumps(enforced_devices)})
		# Create a response using msgpack
		#resp.data = msgpack.packb(response.pretty_print(), use_bin_type=True)
		#resp.content_type = 'application/msgpack'