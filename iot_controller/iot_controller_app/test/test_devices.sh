#!/bin/bash

 USAGE="ANASTACIA USAGE examples:

 	-- Get IoT devices
 		./example_devices.sh [IOT_CONTROLLER_ADDR:PORT] 
 "

if [ $# -eq 0 ]
  then
    printf "$USAGE"
    exit 1
fi

#curl -H "Content-Type: application/json" -X $1 http://$2:8003/api/v1/device/$3?resource=$4
curl -H "Content-Type: application/json" -X GET http://$1/api/v1/devices/




