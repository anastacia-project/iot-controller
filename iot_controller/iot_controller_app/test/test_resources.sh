#!/bin/bash

 USAGE="ANASTACIA USAGE examples:

 --General use--
 	./test_resources.sh GET|PUT [IOT_CONTROLLER_ADDR:PORT] json_input_file

 -- Get resources: 
 	./test_resources.sh GET 10.79.7.34:8003 well-known.json

 -- Set Power off:
 	./test_resources.sh PUT 10.79.7.34:8003 power-off.json

 "

if [ $# -eq 0 ]
  then
    printf "$USAGE"
    exit 1
fi

#curl -H "Content-Type: application/json" -X $1 http://$2:8003/api/v1/device/$3?resource=$4
curl -H "Content-Type: application/json" -X $1 -d @$3 http://$2/api/v1/resource/




