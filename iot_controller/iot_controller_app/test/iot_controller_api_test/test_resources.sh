#!/bin/bash
#__author__ = "Alejandro Molina Zarca"
#__copyright__ = "Copyright 2018, ANASTACIA H2020"
#__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
#__license__ = "GPL"
#__version__ = "0.0.1"
#__maintainer__ = "Alejandro Molina Zarca"
#__email__ = "alejandro.mzarca@um.es"
#__status__ = "Development"

 USAGE="ANASTACIA USAGE examples:

 --General use--
 	./test_resources.sh GET|PUT [IOT_CONTROLLER_ADDR:PORT] json_input_file

 -- Get resources: 
 	./test_resources.sh GET 155.54.95.190:8003 well-known.json

 -- Set Power off:
 	./test_resources.sh PUT 155.54.95.190:8003 power-off.json

 "

if [ $# -eq 0 ]
  then
    printf "$USAGE"
    exit 1
fi

curl -H "Content-Type: application/json" -X $1 -d @$3 http://$2/api/v1/resource/




