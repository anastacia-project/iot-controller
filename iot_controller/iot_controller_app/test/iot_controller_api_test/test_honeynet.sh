#!/bin/bash
#__author__ = "Alejandro Molina Zarca"
#__copyright__ = "Copyright 2018, ANASTACIA H2020"
#__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
#__license__ = "GPL"
#__version__ = "0.0.1"
#__maintainer__ = "Alejandro Molina Zarca"
#__email__ = "alejandro.mzarca@um.es"
#__status__ = "Development"

 USAGE="ANASTACIA USAGE examples:

 	--General use--
 		./test_honeynet.sh [IOT_CONTROLLER_ADDR:PORT]

 	-- Get IoT honeynet
 		./test_honeynet.sh 155.54.95.190:8003 

 "

if [ $# -eq 0 ]
  then
    printf "$USAGE"
    exit 1
fi

curl -H "Content-Type: application/json" -X GET http://$1/api/v1/honeynet/