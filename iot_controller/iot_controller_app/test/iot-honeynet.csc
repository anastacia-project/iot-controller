<?xml version="1.0" encoding="utf-8"?><simconf><project EXPORT="discard">[APPS_DIR]/mrm</project><project EXPORT="discard">[APPS_DIR]/mspsim</project><project EXPORT="discard">[APPS_DIR]/avrora</project><project EXPORT="discard">[APPS_DIR]/serial_socket</project><project EXPORT="discard">[APPS_DIR]/collect-view</project><project EXPORT="discard">[APPS_DIR]/powertracker</project><simulation><title>REST with RPL router</title><randomseed>123456</randomseed><motedelay_us>1000000</motedelay_us><radiomedium>
      org.contikios.cooja.radiomediums.UDGM
      <transmitting_range>15.0</transmitting_range>
      <interference_range>15.0</interference_range>
      <success_ratio_tx>1.0</success_ratio_tx>
      <success_ratio_rx>1.0</success_ratio_rx>
    </radiomedium><events><logoutput>40000</logoutput></events><motetype>se.sics.cooja.mspmote.WismoteMoteType<identifier>1</identifier><description>Wismote RPL Root</description><source>[CONTIKI_DIR]/examples/ipv6/rpl-border-router/border-router.c</source><commands>make border-router.wismote TARGET=wismote</commands><firmware>[CONTIKI_DIR]/examples/ipv6/rpl-border-router/border-router.wismote</firmware><moteinterface>org.contikios.cooja.interfaces.Position</moteinterface><moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface><moteinterface>se.sics.cooja.interfaces.IPAddress</moteinterface><moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface><moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspClock</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspMoteID</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspButton</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.Msp802154Radio</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspDefaultSerial</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspLED</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspDebugOutput</moteinterface></motetype><motetype>se.sics.cooja.mspmote.WismoteMoteType<identifier>3002</identifier><description>Erbium Server</description><source>[CONTIKI_DIR]/examples/er-rest-example/er-example-server.c</source><commands>make er-example-server.wismote TARGET=wismote</commands><firmware>[CONTIKI_DIR]/examples/er-rest-example/er-example-server.wismote</firmware><moteinterface>org.contikios.cooja.interfaces.Position</moteinterface><moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface><moteinterface>se.sics.cooja.interfaces.IPAddress</moteinterface><moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface><moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspClock</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspMoteID</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspButton</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.Msp802154Radio</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspDefaultSerial</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspLED</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspDebugOutput</moteinterface></motetype><motetype>se.sics.cooja.mspmote.WismoteMoteType<identifier>3003</identifier><description>Erbium Server</description><source>[CONTIKI_DIR]/examples/er-rest-example/er-example-server.c</source><commands>make er-example-server.wismote TARGET=wismote</commands><firmware>[CONTIKI_DIR]/examples/er-rest-example/er-example-server.wismote</firmware><moteinterface>org.contikios.cooja.interfaces.Position</moteinterface><moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface><moteinterface>se.sics.cooja.interfaces.IPAddress</moteinterface><moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface><moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspClock</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspMoteID</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspButton</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.Msp802154Radio</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspDefaultSerial</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspLED</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspDebugOutput</moteinterface></motetype><motetype>se.sics.cooja.mspmote.WismoteMoteType<identifier>3004</identifier><description>Erbium Server</description><source>[CONTIKI_DIR]/examples/er-rest-example/er-example-server.c</source><commands>make er-example-server.wismote TARGET=wismote</commands><firmware>[CONTIKI_DIR]/examples/er-rest-example/er-example-server.wismote</firmware><moteinterface>org.contikios.cooja.interfaces.Position</moteinterface><moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface><moteinterface>se.sics.cooja.interfaces.IPAddress</moteinterface><moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface><moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspClock</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspMoteID</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspButton</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.Msp802154Radio</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspDefaultSerial</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspLED</moteinterface><moteinterface>org.contikios.cooja.mspmote.interfaces.MspDebugOutput</moteinterface></motetype><mote><breakpoints></breakpoints><interface_config>se.sics.cooja.interfaces.Position<x>33.260163187353555</x><y>30.643217359962595</y><z>0.0</z></interface_config><interface_config>se.sics.cooja.mspmote.interfaces.MspMoteID<id>1</id></interface_config><motetype_identifier>1</motetype_identifier></mote><mote><breakpoints></breakpoints><interface_config>se.sics.cooja.interfaces.Position<x>35.100895239785295</x><y>39.70574552287428</y><z>0.0</z></interface_config><interface_config>se.sics.cooja.mspmote.interfaces.MspMoteID<id>12290</id></interface_config><motetype_identifier>3002</motetype_identifier></mote><mote><breakpoints></breakpoints><interface_config>se.sics.cooja.interfaces.Position<x>35.100895239785295</x><y>39.70574552287428</y><z>0.0</z></interface_config><interface_config>se.sics.cooja.mspmote.interfaces.MspMoteID<id>12291</id></interface_config><motetype_identifier>3003</motetype_identifier></mote><mote><breakpoints></breakpoints><interface_config>se.sics.cooja.interfaces.Position<x>35.100895239785295</x><y>39.70574552287428</y><z>0.0</z></interface_config><interface_config>se.sics.cooja.mspmote.interfaces.MspMoteID<id>12292</id></interface_config><motetype_identifier>3004</motetype_identifier></mote></simulation><plugin>
    org.contikios.cooja.plugins.SimControl
    <width>280</width>
    <z>1</z>
    <height>160</height>
    <location_x>400</location_x>
    <location_y>0</location_y>
  </plugin><plugin>
    org.contikios.cooja.plugins.Visualizer
    <plugin_config><moterelations>true</moterelations><skin>org.contikios.cooja.plugins.skins.IDVisualizerSkin</skin><skin>org.contikios.cooja.plugins.skins.GridVisualizerSkin</skin><skin>org.contikios.cooja.plugins.skins.TrafficVisualizerSkin</skin><skin>org.contikios.cooja.plugins.skins.UDGMVisualizerSkin</skin><viewport>7.266366960762773 0.0 0.0 7.266366960762773 72.91888287187065 -365.1374754637069</viewport></plugin_config>
    <width>400</width>
    <z>2</z>
    <height>400</height>
    <location_x>1</location_x>
    <location_y>1</location_y>
  </plugin><plugin>
    org.contikios.cooja.plugins.LogListener
    <plugin_config><filter></filter><formatted_time></formatted_time><coloring></coloring></plugin_config>
    <width>1320</width>
    <z>6</z>
    <height>240</height>
    <location_x>400</location_x>
    <location_y>160</location_y>
  </plugin><plugin>
    org.contikios.cooja.plugins.TimeLine
    <plugin_config><mote>0</mote><mote>1</mote><showRadioRXTX></showRadioRXTX><showRadioHW></showRadioHW><showLEDs></showLEDs><zoomfactor>500.0</zoomfactor></plugin_config>
    <width>1720</width>
    <z>5</z>
    <height>166</height>
    <location_x>0</location_x>
    <location_y>709</location_y>
  </plugin><plugin>
    org.contikios.cooja.plugins.Notes
    <plugin_config><notes>Enter notes here</notes><decorations>true</decorations></plugin_config>
    <width>1040</width>
    <z>4</z>
    <height>160</height>
    <location_x>680</location_x>
    <location_y>0</location_y>
  </plugin><plugin>
    org.contikios.cooja.serialsocket.SerialSocketServer
    <mote_arg>0</mote_arg>
    <plugin_config><port>60001</port><bound>true</bound></plugin_config>
    <width>362</width>
    <z>3</z>
    <height>116</height>
    <location_x>20</location_x>
    <location_y>427</location_y>
  </plugin><plugin>
    org.contikios.cooja.plugins.ScriptRunner
    <plugin_config><script>TIMEOUT(86400000);r
         log.log(&quot;first simulation message at time : &quot; + time + &quot;
&quot;);r
         while (true) {r
           log.log(id + &quot; &quot; + time + &quot; &quot; + msg + &quot;
&quot;);r
           YIELD(); /* wait for another mote output */r
         }r</script><active>true</active></plugin_config>
    <width>600</width>
    <z>0</z>
    <height>700</height>
    <location_x>1094</location_x>
    <location_y>104</location_y>
  </plugin></simconf>