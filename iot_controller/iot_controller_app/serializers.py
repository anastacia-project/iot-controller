# # -*- coding: utf-8 -*-
# serielizers.py
"""
This python module implements the policy repository serializers inside the ANASTACIA European Project,
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

from .models import IoTDevice, IoTDeviceIf, IoTDeviceResource, IoTDeviceResourceMethod, IoTDeviceSoftware
from rest_framework import serializers


class IoTDeviceIfSerializer(serializers.ModelSerializer):
	iot_net = serializers.StringRelatedField()

	class Meta:
		model = IoTDeviceIf
		fields = ('if_id','name','mac_addr','ip_addr','iot_net')

class IoTDeviceResourceMethodSerializer(serializers.ModelSerializer):

	class Meta:
		model = IoTDeviceResourceMethod
		fields = ('method','payload')

class IoTDeviceResourceSerializer(serializers.ModelSerializer):

	methods = IoTDeviceResourceMethodSerializer(many=True)

	class Meta:
		model = IoTDeviceResource
		fields = ('resource','description','methods')

class IoTDeviceSoftwareSerializer(serializers.ModelSerializer):

	class Meta:
		model = IoTDeviceSoftware
		fields = ('name','version','port')


class IoTDeviceSerializer(serializers.HyperlinkedModelSerializer):

	resources = IoTDeviceResourceSerializer(many=True)
	ifaces = IoTDeviceIfSerializer(many=True)
	softwares = IoTDeviceSoftwareSerializer(many=True)
	#iot_locations = serializers.StringRelatedField(many=False, read_only=True)
	#iot_softwares = serializers.StringRelatedField(many=True, read_only=True)
	#iot_device_os = serializers.StringRelatedField(many=False, read_only=True)
	#iot_device_ifs = serializers.StringRelatedField(many=True, read_only=True)

	class Meta:
		model = IoTDevice
		fields = ('id','device_id','name','model','x_location','y_location','softwares','ifaces','resources','creation_date')



