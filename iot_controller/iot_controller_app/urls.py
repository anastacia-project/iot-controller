# # -*- coding: utf-8 -*-
# views.py
"""
This python module implements the policy repository urls inside the ANASTACIA European Project,
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"


from django.conf.urls import url, include
from rest_framework import routers
from iot_controller_app import views

#router = routers.DefaultRouter()
#router.register(r'devices', views.IoTDeviceViewSetAPI)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    #url(r'^', include(router.urls)),
    url(r'^devices/$', views.IoTDeviceAPIView.as_view()),
    url(r'^register/$', views.RegisterIoTDeviceAPI.as_view()),
    url(r'^resource/$', views.IoTDeviceResourceAPI.as_view()),
	url(r'^honeynet/$', views.IoTHoneynetAPIView.as_view()),
    #url(r'^control-iot-device/$', views.SetPolicyRefinement.as_view()),
    #url(r'^bootstrapping-iot-device/$', views.SetPolicyRefinement.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

